# What is SMC (Skyrim Mod Combiner)?

SMC is free, fast and light-weight mod combiner for Skyrim, designed to be as universal and user-friendly as possible.
> SMC was initially made as extended GUI wrapper for a batch-file from [Texture Pack Combiner](http://skyrim.nexusmods.com/mods/20801) by **Cestral**,
but uses its own updated batch-files now for compatibily with newer and additional mods.

---

   ![SMC Screenshot](https://lh3.googleusercontent.com/-wCD-AAcUoTo/Uucw66wwxzI/AAAAAAAABAk/q1yHTZAO7wM/s1084/SMC_Screenshot.png "SMC Screenshot")

---

## SMC features:

  + Written fully in AutoIt, so it is **Windows-only**;
  + Comes with a **[7-Zip](http://www.7-zip.org/)** implemented;
  + Uses **SQLite** as an internal database;
  + Works with _archived_ and/or _extracted_ mods;
  + Automatically installs _compatibility patches_, if needed;
  + Dynamically calculates _space required_;
  + Dynamically indicates _mod availability_;
  + Optionally packs the combined output into _NMM-compatible archive_;
  + Optionally _optimizes_ combined textures with **[DDSopt](http://skyrim.nexusmods.com/mods/5755)** and/or _creates BSA-archive_ with **[BSAopt](http://skyrim.nexusmods.com/mods/247)** by **Ethatron**;
  + Automatically creates readable _detailed log_.

---

## Downloading & updating:

  * Latest SMC version can always be found in **[Downloads](https://bitbucket.org/drigger/smc/downloads)** section,  
  * Or you can simply click **[here](https://bitbucket.org/drigger/smc/downloads/SMC_1.0.0.1.7z)**,  
  * And don't worry, SMC will update itself once installed (but you can still check this page every now and then, just in case).  

---

## Latest changes:

* > [1.0.0.1 @ 21.02.2014]  
(E) NMMUseCompression option can now only be selected if NMMCreate is enabled  
* > [1.0.0.0 @ 13.02.2014]  
(+) Main TPC batch-file auto-download  
(F) CPU high consumption bug  
* > [0.9.0.5 @ 03.02.2014]  
(F) A bit more bugfixes  
* > [0.9.0.4 @ 01.02.2014]  
(F) A couple of small bugfixes
* > [0.9.0.3 @ 01.02.2014]  
(+) NMM detection logic enhanced
* > [0.9.0.2 @ 01.02.2014]  
(+) NMM detection status in logs
* > [0.9.0.1 @ 31.01.2014]  
(+) Loading animation  
(E) Renamed some functions & cleaned code a little bit  
* > [0.9.0.0 @ 28.01.2014]  
(E) Application name changed to SMC (Skyrim Mod Combiner)  
(E) Application rebased to https://bitbucket.org/drigger/smc/  

---

## Current to-do list:

* About Horses  
* aMidianBorn Book of Silence  
* aMidianBorn Hide and Studded Hide  
* aMidianBorn Hide of the Savior  
* aMidianBorn imperial light and studded  
* aMidianBorn Iron and Banded Armor  
* aMidianBorn Scaled Armor  
* aMidianBorn Steel Armor  
* aMidianBorn stormcloak officer armour  
* aMidianBorn wolf armor and skyforge weapons  
* Animated clutter  
* Animated Dwemer Lift Load Doors  
* Better Beast Races  
* Better Male Feet  
* Better males  
* Book Covers Skyrim  
* Burn Freeze Shock Effects  
* Coverkhajiits  
* DCE - Realistic Male Face  
* Deadly Spell Impacts  
* Enhanced Blood Textures  
* Enhanced Wetness and Puddles  
* Ethereal Elven Overhaul  
* EWIs little Texture Pack  
* Female Vampires Have Fangs  
* FemFeet Redesigned  
* Footprints  
* Geonox Male Face -Blackbugfix  
* Glyphic Enchantment Effect  
* Hvergelmirs Armor Retexture - HAR  
* Hvergelmirs Shield Retexture - HSR
* Hvergelmirs Steel Armory  
* Improved Foot Wraps for females  
* Improved Weapon Impact EFFECTS Correct Metal  
* Lockpicking interface retex  
* Proper Dark brotherhood armor retex  
* Royal Bloodskal Pattern  
* Skill Interface Retexture  
* SkyFalls and SkyMills - Animated Distant Waterfalls and WindMills  
* Skyrim Improved Puddles - SIP  
* Soul Gems Glow When Full  
* Splash of Rain  
* The 418th Step  
* Thieves Guild Armor HD revival  
* UltraHD - Stormcloak and City Guards  
* Vibrant Auroras  
* Weapon Retexture Project - WRP  
* Windmills Resized  
* WispMother and Quartz Atronach  
* XCE - Dawnguard
* XCE - Warpaint and Dirt  
* XCE - Xenius Character Enhancement  

---

## P.S.

_If you like what I do you can buy me a drink by clicking this:_ [![Donate with PayPal Button](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif "Donate with PayPal")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LVHX9P9F7UHHQ)
