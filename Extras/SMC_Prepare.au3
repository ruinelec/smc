#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Version=Beta
#AutoIt3Wrapper_Outfile_x64=..\SMC_Prepare.exe
#AutoIt3Wrapper_Res_Language=1033
#AutoIt3Wrapper_AU3Check_Parameters=-w 1 -w 2 -w 3 -w 4 -w 5 -w 6 -q
#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 162,
#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 164,
#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 169,
#AutoIt3Wrapper_Run_After=..\_Utilities\mpress.exe -s -r %outx64%
#AutoIt3Wrapper_Run_Tidy=y
#AutoIt3Wrapper_Run_Obfuscator=y
#Obfuscator_Parameters=/striponly /Beta
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#pragma compile(Icon, ..\Include\SMC_Prepare.ico)
#pragma compile(ExecLevel, asInvoker)
#pragma compile(UPX, False)
#pragma compile(AutoItExecuteAllowed, False)
#pragma compile(Compression, 0)
#pragma compile(FileDescription, Skyrim Mod Combiner Preparator)
#pragma compile(ProductName, SMC)
#pragma compile(ProductVersion, 1.0)
#pragma compile(FileVersion, 1.0.0.0)
#pragma compile(LegalCopyright, � dr.)

#include <File.au3>

$AppConfig = @ScriptDir & '\settings.ini'
$TempFile = _TempFile(@TempDir, '~', '.SMC')
IniWriteSection($AppConfig, 'Checksums', '')
$FileList = _FileListToArray(@ScriptDir & '\Pics\', '*.png', 1)
If $FileList <> 0 Then
	For $i = 1 To $FileList[0]
		IniWrite($AppConfig, 'Checksums', 'Pics\' & $FileList[$i], FileGetSize(@ScriptDir & '\Pics\' & $FileList[$i]))
	Next
EndIf
$FileList = _FileListToArray(@ScriptDir & '\Lang\', '*.lng', 1)
If $FileList <> 0 Then
	For $i = 1 To $FileList[0]
		IniWrite($AppConfig, 'Checksums', 'Lang\' & $FileList[$i], FileGetSize(@ScriptDir & '\Lang\' & $FileList[$i]))
	Next
EndIf
$FileList = _FileListToArray(@ScriptDir & '\', '*.bat', 1)
If $FileList <> 0 Then
	For $i = 1 To $FileList[0]
		IniWrite($AppConfig, 'Checksums', $FileList[$i], FileGetSize(@ScriptDir & '\' & $FileList[$i]))
	Next
EndIf
IniWrite($AppConfig, 'System', 'SMCVersionCurrent', '')
IniWrite($AppConfig, 'System', 'PreferredLanguage', '')
IniWrite($AppConfig, 'System', 'PreferredInputPath', '')
IniWrite($AppConfig, 'System', 'PreferredOutputPath', '')
IniWrite($AppConfig, 'System', 'CleanInstall', '')
IniWrite($AppConfig, 'System', 'DDSoptimize', '')
IniWrite($AppConfig, 'System', 'BSAoptPack', '')
IniWrite($AppConfig, 'System', 'NMMCreate', '')
IniWrite($AppConfig, 'System', 'NMMUseCompression', '')
IniWrite($AppConfig, 'System', 'NoAutoUpdate', '')
IniWrite($AppConfig, 'System', 'NoLogging', '')
IniWrite($AppConfig, 'System', 'EditorMode', '')
IniWrite($AppConfig, 'System', 'PreviousOutputPath', '')
IniWrite($AppConfig, 'System', 'PreviousWindowSettings', '')
IniDelete($AppConfig, 'Defined Paths')

If FileExists($AppConfig) Then
	FileDelete($TempFile)
	Global $FileMoveRetries = 0, $FileMoveResult = 0, $iniLineLongest = 0, $iniLine, $iniFileTemp, $iniFileIn, $iniFileOut
	IniFormatSection($AppConfig, 'System')
	IniFormatSection($AppConfig, 'Checksums')
	IniFormatSection($AppConfig, 'Defined Paths')
	IniFormatSection($AppConfig, 'Compatibility Patches')
	$iniFileIn = FileOpen($AppConfig, 0)
	If $iniFileIn = -1 Then Exit
	$iniLineLongest = 0
	While 1
		$iniLine = FileReadLine($iniFileIn)
		If @error = -1 Then ExitLoop
		$iniLine = StringSplit($iniLine, '=')
		If $iniLine[0] > 1 And StringLen(StringStripWS($iniLine[1], 3)) > $iniLineLongest Then $iniLineLongest = StringLen(StringStripWS($iniLine[1], 3))
	WEnd
	FileClose($iniFileIn)
	$iniFileTemp = FileOpen($TempFile, 9)
	$iniFileOut = FileOpen($AppConfig, 0)
	If $iniFileTemp = -1 Or $iniFileOut = -1 Then Exit
	While 1
		$iniLine = FileReadLine($iniFileOut)
		If @error = -1 Then ExitLoop
		$iniLine = StringSplit($iniLine, '=')
		$iniLine[1] = StringStripWS($iniLine[1], 3)
		If $iniLine[0] > 1 Then
			For $i = 1 To $iniLineLongest - StringLen($iniLine[1])
				$iniLine[1] &= ' '
			Next
			$iniLine[1] &= ' = ' & StringReplace(StringReplace((StringRight(StringStripWS($iniLine[2], 3), 1) = ';' ? StringTrimRight(StringStripWS($iniLine[2], 3), 1) : StringRight(StringStripWS($iniLine[2], 3), 1) = '|') ? StringTrimRight(StringStripWS($iniLine[2], 3), 1) : StringStripWS($iniLine[2], 3), ';;', ';'), '; ;', ';')
		EndIf
		FileWriteLine($iniFileTemp, $iniLine[1])
	WEnd
	FileClose($iniFileTemp)
	FileClose($iniFileOut)
	Do
		$FileMoveResult = FileMove($TempFile, $AppConfig, 9)
		If $FileMoveResult <> 1 Then
			$FileMoveRetries += 1
			Sleep(100)
		EndIf
		If $FileMoveRetries > 10 Then Exit
	Until $FileMoveResult = 1
EndIf

Func IniFormatSection($AppConfig, $Section)
	Local $AppConfigSection = IniReadSection($AppConfig, $Section)
	If Not @error Then
		If IniDelete($AppConfig, $Section) = 0 Then Return -1
		$iniLineLongest = 0
		For $i = 1 To $AppConfigSection[0][0]
			If StringLen(StringStripWS($AppConfigSection[$i][0], 3)) > $iniLineLongest Then $iniLineLongest = StringLen(StringStripWS($AppConfigSection[$i][0], 3))
		Next
		Local $iniFileTemp = FileOpen($TempFile, 9)
		If $iniFileTemp = -1 Then Return -1
		FileWriteLine($iniFileTemp, '[' & $Section & ']')
		For $i = 1 To $AppConfigSection[0][0]
			If $AppConfigSection[0][0] > 1 Then
				For $i1 = 1 To $iniLineLongest - StringLen($AppConfigSection[$i][0])
					$AppConfigSection[$i][0] &= ' '
				Next
			EndIf
			FileWriteLine($iniFileTemp, $AppConfigSection[$i][0] & ' = ' & (StringRight(StringStripWS($AppConfigSection[$i][1], 3), 1) = ';' ? StringTrimRight(StringStripWS($AppConfigSection[$i][1], 3), 1) : StringRight(StringStripWS($AppConfigSection[$i][1], 3), 1) = '|' ? StringTrimRight(StringStripWS($AppConfigSection[$i][1], 3), 1) : StringStripWS($AppConfigSection[$i][1], 3)))
		Next
		FileClose($iniFileTemp)
	EndIf
EndFunc   ;==>IniFormatSection
