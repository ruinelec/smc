#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Version=Beta
#AutoIt3Wrapper_Outfile_x64=..\SMC_Merge.exe
#AutoIt3Wrapper_Res_Language=1033
#AutoIt3Wrapper_AU3Check_Parameters=-w 1 -w 2 -w 3 -w 4 -w 5 -w 6 -q
#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 162,
#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 164,
#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 169,
#AutoIt3Wrapper_Run_After=..\_Utilities\mpress.exe -s -r %outx64%
#AutoIt3Wrapper_Run_Tidy=y
#AutoIt3Wrapper_Run_Obfuscator=y
#Obfuscator_Parameters=/striponly /Beta
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#pragma compile(Icon, ..\Include\SMC_Merge.ico)
#pragma compile(ExecLevel, asInvoker)
#pragma compile(UPX, False)
#pragma compile(AutoItExecuteAllowed, False)
#pragma compile(Compression, 0)
#pragma compile(FileDescription, Skyrim Mod Combiner Merger)
#pragma compile(ProductName, SMC)
#pragma compile(ProductVersion, 1.0)
#pragma compile(FileVersion, 1.0.0.0)
#pragma compile(LegalCopyright, � dr.)

#include <File.au3>
#include <Array.au3>
#include <Misc.au3>

If @Compiled Then
	$Path = @ScriptDir & '\'
	$Advanced = 0
Else
	$Path = '..\'
	$Advanced = 1
EndIf

BatchMerge($Path & 'TexturePackCombiner_v1.95b.bat', $Path & 'TexturePackCombiner_Update.bat', $Path & 'TexturePackCombiner_HF_Update.bat', $Path & 'TexturePackCombiner_DG_Update.bat', $Path & 'TexturePackCombiner_DB_Update.bat', $Path & 'TexturePackCombiner_Merged.bat')
BatchMerge($Path & 'TexturePackCombinerLIGHT_v1.95b.bat', $Path & 'TexturePackCombinerLIGHT_Update.bat', $Path & 'TexturePackCombinerLIGHT_HF_Update.bat', $Path & 'TexturePackCombinerLIGHT_DG_Update.bat', $Path & 'TexturePackCombinerLIGHT_DB_Update.bat', $Path & 'TexturePackCombinerLIGHT_Merged.bat')

Func BatchMerge($Input, $InputUpdate, $InputUpdateHF, $InputUpdateDG, $InputUpdateDB, $Output)
	If FileExists($Input) Then
		$BatchContents = FileReadToArray($Input)
		For $i = 0 To UBound($BatchContents) - 1
			If StringInStr($BatchContents[$i], '\_byoh\') <> 0 Or StringInStr($BatchContents[$i], 'Hearthfire') <> 0 Or StringInStr($BatchContents[$i], '\dlc01\') <> 0 Or StringInStr($BatchContents[$i], 'Dawnguard') <> 0 Or StringInStr($BatchContents[$i], '\dlc02\') <> 0 Or StringInStr($BatchContents[$i], 'Dragonborn') <> 0 Or StringInStr($BatchContents[$i], 'call:combine') = 0 Then $BatchContents[$i] = ''
		Next
		If FileExists($InputUpdate) Then
			$SMCbatUpdateContents = FileReadToArray($InputUpdate)
			_ArrayConcatenate($BatchContents, $SMCbatUpdateContents)
		EndIf
		If FileExists($InputUpdateHF) Then
			$SMCbatUpdateContents = FileReadToArray($InputUpdateHF)
			_ArrayConcatenate($BatchContents, $SMCbatUpdateContents)
		EndIf
		If FileExists($InputUpdateDG) Then
			$SMCbatUpdateContents = FileReadToArray($InputUpdateDG)
			_ArrayConcatenate($BatchContents, $SMCbatUpdateContents)
		EndIf
		If FileExists($InputUpdateDB) Then
			$SMCbatUpdateContents = FileReadToArray($InputUpdateDB)
			_ArrayConcatenate($BatchContents, $SMCbatUpdateContents)
		EndIf
		_ArrayReverse($BatchContents, 1)
		$BatchContents = _ArrayUnique($BatchContents)
		_ArrayReverse($BatchContents, 1)
		If $Advanced = 1 Then
			Local $ModArray[1][2] = [[0, 0]]
			For $i = 0 To UBound($BatchContents) - 1
				If StringLeft($BatchContents[$i], 12) = 'call:combine' Then
					$BatchRecords = StringSplit($BatchContents[$i], '"')
					$BatchRecordsFromSplit = StringSplit($BatchRecords[2], '\')
					$BatchRecordsFrom = ''
					$AllowPathCreation = 0
					For $i1 = 3 To $BatchRecordsFromSplit[0]
						If StringInStr('textures meshes', $BatchRecordsFromSplit[$i1]) Or StringRight($BatchRecordsFromSplit[$i1], 4) = '.bsa' Or StringRight($BatchRecordsFromSplit[$i1], 4) = '.esp' Or $AllowPathCreation = 1 Then
							$AllowPathCreation = 1
							$BatchRecordsFrom &= '\' & $BatchRecordsFromSplit[$i1]
						EndIf
					Next
					$ModArray[0][0] += 1
					ReDim $ModArray[UBound($ModArray) + 1][2]
					$ModArray[$ModArray[0][0]][0] = $BatchRecordsFromSplit[2]
					$ModArray[$ModArray[0][0]][1] = $BatchRecordsFrom
				EndIf
			Next
			_ArrayDelete($ModArray, 0)
			$ModNames = _ArrayUnique($ModArray, 1)
			_ArrayDelete($ModNames, 0)
			_ArraySort($ModNames)
			$ModPaths = _ArrayUnique($ModArray, 2)
			_ArrayDelete($ModPaths, 0)
			Local $ModPathsUnique[UBound($ModPaths)]
			For $i1 = UBound($ModPaths) - 1 To 0 Step -1
				For $i2 = UBound($ModArray) - 1 To 0 Step -1
					If StringCompare($ModPaths[$i1], $ModArray[$i2][1]) = 0 Then
						$ModPathsUnique[$i1] = $ModArray[$i2][0]
						ExitLoop
					EndIf
				Next
			Next
			$ModNamesUnique = _ArrayUnique($ModPathsUnique)
			_ArrayDelete($ModNamesUnique, 0)
			_ArraySort($ModNamesUnique)
			For $i1 = 0 To UBound($ModNames) - 1
				For $i2 = 0 To UBound($ModNamesUnique) - 1
					If StringCompare($ModNames[$i1], $ModNamesUnique[$i2]) = 0 Then
						$ModNames[$i1] = ''
						ExitLoop
					EndIf
				Next
			Next
			$ModNames = _ArrayUnique($ModNames)
			_ArrayDelete($ModNames, 0)
			_ArraySort($ModNames)
			_ArrayDelete($ModNames, 0)
			_ArrayDisplay($ModNames)
		EndIf
		$FileOutput = FileOpen($Output, 10)
		If $FileOutput <> -1 Then
			For $i = 0 To UBound($BatchContents) - 1
				If StringLeft($BatchContents[$i], 12) = 'call:combine' Then FileWriteLine($FileOutput, $BatchContents[$i])
			Next
		EndIf
	EndIf
EndFunc   ;==>BatchMerge
