#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Version=Beta
#AutoIt3Wrapper_Outfile_x64=..\SMC_ListFolder.exe
#AutoIt3Wrapper_Res_Language=1033
#AutoIt3Wrapper_AU3Check_Parameters=-w 1 -w 2 -w 3 -w 4 -w 5 -w 6 -q
#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 162,
#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 164,
#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 169,
#AutoIt3Wrapper_Run_After=..\_Utilities\mpress.exe -s -r %outx64%
#AutoIt3Wrapper_Run_Tidy=y
#AutoIt3Wrapper_Run_Obfuscator=y
#Obfuscator_Parameters=/striponly /Beta
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#pragma compile(Icon, ..\Include\SMC_ListFolder.ico)
#pragma compile(ExecLevel, asInvoker)
#pragma compile(UPX, False)
#pragma compile(AutoItExecuteAllowed, False)
#pragma compile(Compression, 0)
#pragma compile(FileDescription, Skyrim Mod Combiner List Folder)
#pragma compile(ProductName, SMC)
#pragma compile(ProductVersion, 0.9)
#pragma compile(FileVersion, 0.9.0.1)
#pragma compile(LegalCopyright, � dr.)

#include <..\Include\RecFileListToArray.au3>

Global $hFileOpen = FileOpen('_PathList.txt', 10)
Global $List = _RecFileListToArray(@ScriptDir, '*', 1, 1, 1, 1)
For $i = 1 To $List[0]
	Global $ListPath = StringLeft($List[$i], StringInStr($List[$i], '\', 0, -1))
	If $ListPath Then
		If StringInStr($ListPath, 'Screenshot') = 0 And StringLeft($ListPath, StringInStr($ListPath, '\', 0, 1)) <> 'images\' And StringLeft($ListPath, StringInStr($ListPath, '\', 0, 1)) <> 'Fomod\' And StringLeft($ListPath, StringInStr($ListPath, '\', 0, 1)) <> '_\' And StringInStr('txt ini inf exe .db .7z rar zip', StringRight($List[$i], 3)) = 0 Then
			If StringLeft($List[$i], 7) = 'meshes\' Or StringLeft($List[$i], 9) = 'textures\' Then
				FileWriteLine($hFileOpen, 'call:combine "Optional Mods\' & StringTrimLeft(@ScriptDir, StringInStr(@ScriptDir, '\', 0, -1)) & '\' & $List[$i] & '" "Combined\Data\' & $ListPath & '"')
			Else
				FileWriteLine($hFileOpen, 'call:combine "Optional Mods\' & StringTrimLeft(@ScriptDir, StringInStr(@ScriptDir, '\', 0, -1)) & '\' & $List[$i] & '" "' & StringReplace('Combined\Data\' & StringTrimLeft($ListPath, StringInStr($ListPath, '\', 0, 1)), '\Data\Data\', '\Data\') & '"')
			EndIf
		EndIf
	Else
		If StringLeft($List[$i], 9) <> 'SMC' And StringInStr('txt ini inf exe .db .7z rar zip', StringRight($List[$i], 3)) = 0 Then
			FileWriteLine($hFileOpen, 'call:combine "Optional Mods\' & StringTrimLeft(@ScriptDir, StringInStr(@ScriptDir, '\', 0, -1)) & '\' & $List[$i] & '" "Combined\Data\"')
		EndIf
	EndIf
Next
FileClose($hFileOpen)

