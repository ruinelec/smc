#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Version=Beta
#AutoIt3Wrapper_Outfile_x64=..\SMC_ListDiff.exe
#AutoIt3Wrapper_Res_Language=1033
#AutoIt3Wrapper_AU3Check_Parameters=-w 1 -w 2 -w 3 -w 4 -w 5 -w 6 -q
#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 162,
#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 164,
#AutoIt3Wrapper_Run_After=..\_Utilities\ResHacker.exe -delete %outx64%, %outx64%, Icon, 169,
#AutoIt3Wrapper_Run_After=..\_Utilities\mpress.exe -s -r %outx64%
#AutoIt3Wrapper_Run_Tidy=y
#AutoIt3Wrapper_Run_Obfuscator=y
#Obfuscator_Parameters=/striponly /Beta
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#pragma compile(Icon, ..\Include\SMC_ListDiff.ico)
#pragma compile(ExecLevel, asInvoker)
#pragma compile(UPX, False)
#pragma compile(AutoItExecuteAllowed, False)
#pragma compile(Compression, 0)
#pragma compile(FileDescription, Skyrim Mod Combiner List Diff)
#pragma compile(ProductName, SMC)
#pragma compile(ProductVersion, 0.9)
#pragma compile(FileVersion, 0.9.0.1)
#pragma compile(LegalCopyright, � dr.)

#include <File.au3>
#include <Array.au3>
#include <Misc.au3>

Global $InputArray, $OutputArray
If _IsPressed(11) And FileExists('E:\Dropbox\=SMC\TexturePackCombiner_Merged.bat') Then
	$Input = 'E:\Dropbox\=SMC\TexturePackCombiner_Merged.bat'
Else
	$Input = FileOpenDialog('Input File Select', @ScriptDir, 'Batch files (*.bat;*.txt)', 3)
EndIf
$InputArray = FileReadToArray($Input)
_ArraySort($InputArray)
$Input = StringTrimRight(StringTrimLeft($Input, StringInStr($Input, '\', 0, -1)), 4)
If _IsPressed(11) And FileExists(@ScriptDir & '\_PathList.txt') Then
	$Output = @ScriptDir & '\_PathList.txt'
Else
	$Output = FileOpenDialog('Output File Select', @ScriptDir, 'Batch files (*.bat;*.txt)', 3)
EndIf
$OutputArray = FileReadToArray($Output)
_ArraySort($OutputArray)
$Output = StringTrimRight(StringTrimLeft($Output, StringInStr($Output, '\', 0, -1)), 4)

For $i = 0 To UBound($InputArray) - 1
	For $j = 0 To UBound($OutputArray) - 1
		If $InputArray[$i] = $OutputArray[$j] Then
			_ArrayDelete($OutputArray, $j)
			ExitLoop
		EndIf
	Next
Next

_FileWriteFromArray(@ScriptDir & '\_' & $Input & '-' & $Output & '.txt', $OutputArray, 0)
